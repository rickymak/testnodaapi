
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
const chalk = require('chalk');
var dbConfig = require('./config/database');
var index = require('./routes/index');
var app = express();
app.disable('etag');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept-Language, Authorization");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    if ('OPTIONS' === req.method) {
        res.sendStatus(200);
    } else {
        next();
    }
});
if (process.env.NODE_ENV === 'production') {
    mongoose.connect(dbConfig.database.production)
    .then(() => console.log(chalk.green.inverse.bold('Droduction MongoDB Connected Successfully >>>>>>>')))
    .catch(err => console.error(chalk.red.inverse.bold('could not connected to mongodb..', err)));
} else {
    mongoose.connect(dbConfig.database.development)
        .then(() => console.log(chalk.green.inverse.bold('Development MongoDB Connected Successfully')))
        .catch(err => console.error(chalk.red.inverse('could not connected to mongodb..', err)));
}
app.get('/get', function (req, res) {
    res.send('Hello World Ricky') 
  })
app.use('/api', index);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
app.listen(4100, () => { console.log(chalk.blueBright.underline.bold("Server Port Start on 4100")) }) 
