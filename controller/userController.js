
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
// var moment = require('moment');
var sceret = require('../config/constant');
var userModel = require('../models/user');




module.exports.createUser = function (req, res) {   
    if (!req.body.email || !req.body.password || !req.body.device_token) {
        return res.status(400).json({'message': 'Invalid parameters'});
    }
    userModel.find({ email: req.body.email }, function(err, resultUser) {
        if(err) {
            return res.status(500).json({'message': "Internal server error"});
        }
        if(!resultUser.length) {
            bcrypt.hash(req.body.password, 10, function (err, hash, next) {
                if (err) {
                    return next(err);
                }
                var userObject = new userModel({
                    fname: req.body.fname !== null ? req.body.fname : null,
                    lname: req.body.lname !== null ? req.body.lname : null,
                    email: req.body.email,
                    password: hash,
                    phone_number: req.body.phone_number !== null ? req.body.phone_number : null,
                    date_of_birth: req.body.date_of_birth !== null ? req.body.date_of_birth : null,
                    gender: req.body.gender !== null ? req.body.gender : null,
                    image: req.body.image,
                    device_token: req.body.device_token
                });

                userObject.save(function (err, resultUserCreated) {
                    if (err) {
                        if (err.code === 11000) {
                            return res.status(500).json({'message': "Duplicate entry..."});
                        }
                        return res.status(500).json({'message': 'Internal server error', error: err});                        
                    }
                    var token = jwt.sign({user_id: resultUserCreated._id}, sceret.secret);
                    resultUserCreated.password = undefined;
                    return res.status(200).json({'message': "User created successfully", user: resultUserCreated, token: token, imageUrl: process.env.S3_URL});
                });
            });
        } else {
            if(resultUser.social_id != null) {
                return res.status(403).json({'message': "Email already used with social login"});
            } else {
                return res.status(403).json({'message': "Email already used"});
            }
        }
    });
};


module.exports.getUserById = async (req, res) => {
    console.log("req.params.userId>>",req.body.userId)
    userModel.find({ _id: req.params.userId }, function(err, resultUser) {
        console.log('resultUser',resultUser)
        if(err) {
            if(resultUser == undefined){
                return res.status(500).json({'message': "User not be found given by Id" + req.params.userId});
            }
            return res.status(500).json({'message': "Internal server error"});
        }
        else if(resultUser != undefined){
            return res.status(200).json({'message': "User retrive successfully", user: resultUser});
        }
    })
}

module.exports.editUser = function (req, res) {
    userModel.findByIdAndUpdate({_id: req.params.userId}, req.body, {new : true, runValidators: true}, function (err, resultUpdatedUser) {
        if (err) {
            return res.status(400).json({'message': 'Internal server error.'});
        }
        return res.status(200).json({'message': "User updated successfully", user: resultUpdatedUser, imageUrl: process.env.S3_URL});
    });
};




