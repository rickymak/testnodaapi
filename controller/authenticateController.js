var sceret = require('../config/constant');
var UserModel = require('../models/user');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

module.exports.authenticateUser = function (req, res) {
    if (!req.body.email || !req.body.password ) {
        return res.status(400).json({message: 'Invalid parameters'});
    }

    let query = {       
            "email": req.body.email
    }

    UserModel.findOne(query, function (err, resultUser) {
        if (err) {
            return res.status(500).json({'message': "Internal erver error..."});
        }
        if (resultUser !== null && resultUser.password !== null) {
            bcrypt.compare(req.body.password, resultUser.password, function (err, result) {
                if (err) {
                    return res.status(500).json({'message': "Internal erver error..."});
                }
                if (result === true) {
                    var token = jwt.sign({user_id: resultUser._id}, sceret.secret);
                    delete resultUser['password'];

                 //   let update = { $addToSet : { device_token: req.body.deviceToken }, $set : { isFirstTimeLogin: false } };
                    

                    return res.status(200).json({'message': "Login successfully", user: resultUser, token: token});
                } else {
                    return res.status(403).json({'message': "Password not valid"});
                }
            });
        } else {
            return res.status(403).json({'message': "User not valid please try with social login"});
        }
    });
};

