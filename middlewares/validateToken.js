var jwt = require('jsonwebtoken');
var sceret = require('../config/constant');

module.exports = function (req, res, next) {
  var token = req.headers.authorization;
  token = req.headers.authorization.split(" ")[1];
  if (token) {
    jwt.verify(token, sceret.secret, function (err, decoded) {    
      if (err) {
        return res.status(403).json({ 'success': 'false', 'message': 'Invalid token' });
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    return res.status(403).json({ 'success': 'false', 'message': 'Invalid request' });
  }
};
