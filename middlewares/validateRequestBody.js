module.exports.validateFnameLnameDreamIdInRequestBody = function(req, res, next) {
    if(!req.body.fname || !req.body.lname || !req.body.dream_id) {
        return res.status(400).json({message: 'Invalid parameters'});
    } else {
        next();
    }
}

module.exports.validateLogoutBody = function(req, res, next) {
    if(!req.body.device_token) {
        return res.status(400).json({message: 'Invalid parameters'});
    } else {
        next();
    }
}

module.exports.validateNotificationStatusUpdate = function(req, res, next) {
    if(req.body.status == undefined) {
        return res.status(400).json({message: 'Invalid parameters'});
    } else {
        next();
    }
}

module.exports.validateDeleteDreams = function(req, res, next) {
    if(!req.body.dreamsIds) {
        return res.status(400).json({message: 'Invalid parameters'});
    } else {
        next();
    }
}

module.exports.validateUpdateCategorySequence = function(req, res, next) {
    if(!req.body.ids) {
        return res.status(400).json({message: 'Invalid parameters'});
    } else {
        next();
    }
}

module.exports.validateBlockUser = function(req, res, next) {
    if(!req.body.userId) {
        return res.status(400).json({message: 'User id not found'});
    } else {
        next();
    }
}

module.exports.validateUnBlockUser = function(req, res, next) {
    if(!req.body.blockedId) {
        return res.status(400).json({message: 'Unblock id not found'});
    } else {
        next();
    }
}

module.exports.validateReport = function(req, res, next) {
    if(req.params.content === "dream") {
        if(!req.body.dreamId) {
            return res.status(400).json({message: 'Dream id is missing'});
        } else {
            next();
        }
    } else if(req.params.content === "user") {
        if(!req.body.userId) {
            return res.status(400).json({message: 'User id is missing'});
        } else {
            next();
        }
    } else if(req.params.content === "action") {
        if(!req.body.actionId) {
            return res.status(400).json({message: 'Action id is missing'});
        } else {
            next();
        }
    } else if(req.params.content === "comment") {
        if(!req.body.commentId) {
            return res.status(400).json({message: 'Comment id is missing'});
        } else {
            next();
        }
    } else {
        return res.status(406).json({message: 'Report content is missing'});
    }
}