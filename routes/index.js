var express = require('express');
var router = express.Router();
var authenticateController = require('../controller/authenticateController');
var userController = require('../controller/userController');
var validateToken = require('../middlewares/validateToken');
router.post('/create_user', userController.createUser);
router.post('/login', authenticateController.authenticateUser);
router.get('/user/:userId',validateToken , userController.getUserById);
module.exports = router;
