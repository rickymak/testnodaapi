var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    fname: {type: String, default: null, validate: /[a-zA-Z]/},
    lname: {type: String, default: null, validate: /[a-zA-Z]/},    
    phone_number: {type: String, default: null},
    date_of_birth: {type: Date, default: null},
    gender: {type: String, require},
    password: {type: String, default: null},   
    image: {type: String, default: null},
    email: {type: String, validate :/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/},
    is_active: {type: Boolean, default: true},
    device_token: [{type: String, default: null}],
    isFirstTimeLogin: {type: Boolean, default: true},
    is_deleted: {type: Boolean, default: false},
    is_created: {type: Date},
    is_updated: {type: Date}
});


userSchema.pre('save', function (next) {
    var currentDate = new Date();
    this.is_updated = currentDate;
    if (!this.is_created) {
        this.is_created = currentDate;
    }
    next();
});

userSchema.statics.getUserById = function(userId) {
    return this.findOne({
        _id: mongoose.Types.ObjectId(userId)
    }).select({
        "_id": 1,
        "is_created": 1,
        "is_updated": 1,
        "email": 1,
        "is_active": 1,
        "image": 1,
        "gender": 1,
        "date_of_birth": 1,
        "phone_number": 1,
        "social_id": 1,
        "lname": 1,
        "fname": 1
    }).exec();
}

// userSchema.statics.deleteUserById = function(userId) {
//     return this.findByIdAndUpdate(userId, {'$set': { is_deleted: true }}, {new: true}).exec();
// }

var User = mongoose.model('User', userSchema);
module.exports = User;

